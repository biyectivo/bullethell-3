if (live_enabled && live_call()) return live_result;

/// @description Dashing duration
currently_dashing = false;
dash_direction_horizontal = 0;
dash_direction_vertical = 0;
