if (live_enabled && live_call()) return live_result;

event_inherited();


#region Player-specific attributes

	currently_dashing = false;
	dash_duration = 10;
	dash_direction_horizontal = 0;
	dash_direction_vertical = 0;
	dash_speed_per_second = entity_speed_per_second * 4;
	
	player_position = array_create(DASH_EFECT_LENGTH);
	
	current_weapon = obj_Bullet_Basic;
	
	// Weapon modifiers
	bullet_number_modifier = 1;				// Actual number
	bullet_speed_per_second_modifier = 1;	// Multiplier
	bullet_drift_function_modifier = -1;	// Actual function
	bullet_damage_modifier = 1;				// Multiplier
	bullet_cooldown_modifier = 1;			// Multiplier
	bullet_friendly_modifier = true;		// Actual value
	

#endregion

#region Player-specific methods

	GetInput = function() {
		if (Game.player_controls[player_index] == -1) { // Keyboard
			pressed_move_horizontally = keyboard_check(Game.control_move_right[player_index]) - keyboard_check(Game.control_move_left[player_index]);
			pressed_move_vertically = keyboard_check(Game.control_move_down[player_index]) - keyboard_check(Game.control_move_up[player_index]);
			pressed_dash = keyboard_check_pressed(Game.control_dash[player_index]);
			pressed_shoot_horizontally = keyboard_check(Game.control_shoot_right[player_index]) - keyboard_check(Game.control_shoot_left[player_index]);
			pressed_shoot_vertically = keyboard_check(Game.control_shoot_down[player_index]) - keyboard_check(Game.control_shoot_up[player_index]);
		}
		else { // Gamepad
			var _dev = Game.player_controls[player_index];				
			pressed_move_horizontally = (gamepad_axis_value(_dev, Game.control_move_right[player_index]) > GP_THRESHOLD) - (gamepad_axis_value(_dev, Game.control_move_left[player_index]) < -GP_THRESHOLD);
			pressed_move_vertically = (gamepad_axis_value(_dev, Game.control_move_down[player_index]) > GP_THRESHOLD) - (gamepad_axis_value(_dev, Game.control_move_up[player_index]) < -GP_THRESHOLD);
			pressed_dash = gamepad_button_check_pressed(_dev, Game.control_dash[player_index]);
			pressed_shoot_horizontally = gamepad_button_check(_dev, Game.control_shoot_right[player_index]) - gamepad_button_check(_dev, Game.control_shoot_left[player_index]);
			pressed_shoot_vertically = gamepad_button_check(_dev, Game.control_shoot_down[player_index]) - gamepad_button_check(_dev, Game.control_shoot_up[player_index]);
		}
	}

	CanMove = function() {
		return !currently_dashing && entity_hp > 0;
	}

	Move = function() {
		var _level_border_radius = Game.current_circle_radius - RADIUS_THRESHOLD;
		if (point_in_circle(x+pressed_move_horizontally * entity_speed_per_second, y, room_width/2, room_height/2, _level_border_radius)) {
			x += pressed_move_horizontally * entity_speed_per_second;
		}
	
		if (point_in_circle(x, y+pressed_move_vertically * entity_speed_per_second, room_width/2, room_height/2, _level_border_radius)) {
			y += pressed_move_vertically * entity_speed_per_second;
		}
	}

	CanDash = function() {
		return !currently_dashing && pressed_dash && entity_hp > 0;
	}

	Dash = function() {
		if (CanDash()) {
			currently_dashing = true;
			dash_direction_horizontal = pressed_move_horizontally;
			dash_direction_vertical = pressed_move_vertically;
			alarm[0] = dash_duration;
		}
	
		if (currently_dashing) {
			var _level_border_radius = Game.current_circle_radius - RADIUS_THRESHOLD;
			if (point_in_circle(x+dash_direction_horizontal * dash_speed_per_second, y, room_width/2, room_height/2, _level_border_radius)) {
				x += dash_direction_horizontal * dash_speed_per_second;
			}
	
			if (point_in_circle(x, y+dash_direction_vertical * dash_speed_per_second, room_width/2, room_height/2, _level_border_radius)) {
				y += dash_direction_vertical * dash_speed_per_second;
			}
		}
	}
	
	Target = function() {
	
	}

	CanShoot = function() {
		return !currently_dashing && alarm[1] <= 0 && entity_hp > 0;
	}
	
	Shoot = function() {
		if (pressed_shoot_horizontally != 0) {
			sprite_index = spr_Player_Shoot_Horizontally;
			image_xscale = (pressed_shoot_horizontally == 1) ? -1 : 1;
			var _pos = (pressed_shoot_horizontally == 1) ? bbox_right : bbox_left;
			var _dir = (pressed_shoot_horizontally == 1) ? 0 : 180;
			var _id = instance_create_layer(_pos, y, "lyr_Instances", current_weapon);			
		}
		else if (pressed_shoot_vertically != 0) {
			sprite_index = (pressed_shoot_vertically == 1) ? spr_Player_Shoot_Down : spr_Player_Shoot_Up;
			image_xscale = 1;
			var _pos = (pressed_shoot_vertically == 1) ? bbox_bottom : bbox_top;
			var _dir = (pressed_shoot_vertically == 1) ? 270 : 90;
			var _id = instance_create_layer(x, _pos, "lyr_Instances", current_weapon);				
		}
		else {
			sprite_index = spr_Player_IdleMove;
			image_xscale = 1;
			var _id = noone;
			var _pos = noone;
			var _dir = noone;
		}
		
		if (_id != noone) {
			with (_id) {				
				// Modifiers
				bullet_speed_per_second *= other.bullet_speed_per_second_modifier;
				if (other.bullet_drift_function_modifier != -1)		bullet_drift_function = other.bullet_drift_function_modifier;
				bullet_damage *= other.bullet_damage_modifier;
				bullet_cooldown = max(round(bullet_cooldown * other.bullet_cooldown_modifier), 1);
				if (other.bullet_friendly_modifier != -1)			bullet_friendly = other.bullet_friendly_modifier;
				
				bullet_entity_id = other.id;
				bullet_friendly = other.friendly;
				bullet_direction = _dir;
				other.alarm[1] = bullet_cooldown;				
			}
		}
	}

	IsInvincible = function() {
		return currently_dashing;
	}
	
#endregion

#region Light

	light.blend = c_white;

#endregion
