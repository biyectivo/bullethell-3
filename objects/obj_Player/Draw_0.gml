if (live_enabled && live_call()) return live_result;

// For some reason, pal_swap_set does not work with asset_get_index...
var _surface = (player_index == 0) ? Game.palette_surface_Player1 : Game.palette_surface_Player2;
pal_swap_set(_surface, 1, true);

if (currently_dashing) {
	for (var _i=0; _i<DASH_EFECT_LENGTH; _i++) {
		draw_sprite_ext(sprite_index, image_index, player_position[_i].x, player_position[_i].y, image_xscale, image_yscale, image_angle, image_blend, _i/DASH_EFECT_LENGTH);
	}
}
else {
	draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
}

pal_swap_reset();

event_inherited();
