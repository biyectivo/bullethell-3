if (live_enabled && live_call()) return live_result;

// Store previous player positions for dash effect
array_push(player_position, new Coord(x, y));
array_resize_from_end(player_position, DASH_EFECT_LENGTH);

event_inherited();
