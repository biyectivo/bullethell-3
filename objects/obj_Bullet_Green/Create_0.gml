if (live_enabled && live_call()) return live_result;

event_inherited();

bullet_speed_per_second = 400 * PX_PER_SEC;

light.blend = c_lime;

ScaleRotate = function() {
	image_angle += 15;
}
