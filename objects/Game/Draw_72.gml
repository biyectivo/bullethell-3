if (live_enabled && live_call()) return live_result;


// Recreate palette swapper surfaces if needed
if (!surface_exists(palette_surface_Player1)) {
	palette_surface_Player1 = surface_create(sprite_get_width(spr_palette_swap_Player1), sprite_get_height(spr_palette_swap_Player1));		
	surface_set_target(palette_surface_Player1);
	draw_sprite(spr_palette_swap_Player1, 0, 0, 0);
	surface_reset_target();	
}

if (!surface_exists(palette_surface_Player2)) {
	palette_surface_Player2 = surface_create(sprite_get_width(spr_palette_swap_Player2), sprite_get_height(spr_palette_swap_Player2));
	surface_set_target(palette_surface_Player2);
	draw_sprite(spr_palette_swap_Player2, 0, 0, 0);
	surface_reset_target();	
}

var _x = current_game_step % sprite_get_width(spr_Background);

draw_sprite_tiled(spr_Background, 0, _x, -_x);
draw_circle_color(room_width/2, room_height/2, current_circle_radius, #27283f, #27283f, false);
draw_circle_color(room_width/2, room_height/2, current_circle_radius-RADIUS_THRESHOLD, #323353, #323353, false);
