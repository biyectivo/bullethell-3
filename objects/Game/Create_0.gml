InitializeGameVariables();

draw_set_circle_precision(64);

// Retro palette swapper initialization
if (os_browser == browser_not_a_browser) {
	pal_swap_init_system(shd_pal_swapper);
}
else {
	pal_swap_init_system(shd_pal_swapper, shd_pal_html_sprite, shd_pal_html_surface);
}
