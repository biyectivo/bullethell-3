if (live_enabled && live_call()) return live_result;

// Update step
current_game_step++;

// Update gamepad count
UpdateGamepads();

// God Mode
show_debug_overlay(debug);


if (god_mode) {	
	
	if (keyboard_check_pressed(vk_escape))		game_restart();	
	if (keyboard_check_pressed(vk_tab))			Game.debug = !Game.debug;
	
}


