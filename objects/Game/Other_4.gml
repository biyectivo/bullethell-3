// Fix resolution
var _tags = asset_get_tags(room, asset_room);
if (array_length(_tags) > 0 && _tags[0] == "game") {
	camera_set_view_pos(CAM, 0, 0);
	camera_set_view_size(CAM, room_width, room_height);
	window_set_size(room_width, room_height);
	surface_resize(application_surface, room_width, room_height);
}


// Spawn player
if (room == room_Test) {
	
	// Reset variables
	ResetGameVariables();
	
	// Create players	
	with (instance_create_layer(room_width/2-2*sprite_get_width(spr_Player_IdleMove), room_height/2, "lyr_Instances", obj_Player)) {
		player_index = 0;
	}
	with (instance_create_layer(room_width/2+2*sprite_get_width(spr_Player_IdleMove), room_height/2, "lyr_Instances", obj_Player)) {
		player_index = 1;
	}
	
	// Create enemy
	instance_create_layer(room_width/2, 200, "lyr_Instances", obj_Enemy_Basic);
	
	// Light
	instance_create_layer(room_width/2, room_height/2, "lyr_Light", obj_Light);
	
}
