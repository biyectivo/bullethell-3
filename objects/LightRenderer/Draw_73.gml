if (live_enabled && live_call()) return live_result;
/// @description Update and draw renderer
renderer.Update(0, 0, room_width, room_height);
renderer.Draw(0, 0);
