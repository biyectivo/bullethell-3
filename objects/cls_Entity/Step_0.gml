if (live_enabled && live_call()) return live_result;

GetInput();

if (CanMove()) {
	Move();
}

Dash();

// Tick waves
var _n = array_length(waves);
if (_n>0) {
	current_wave_time++;
	
	waves[current_wave].tick();
	
	// Process next wave
	if (wave_end_condition_functions[current_wave]()) {
		if (current_wave < _n-1) {
			current_wave++;
			current_wave_time = 0;
		}
		else {
			if (wave_loop_behavior == WAVE_LOOP_BEHAVIOR.END) {
				waves = [];
				current_wave = 0;
				current_wave_time = 0;
			}
			else if (wave_loop_behavior == WAVE_LOOP_BEHAVIOR.LOOP) {
				current_wave = 0;
				current_wave_time = 0;
			}
			// Do nothing if STICK
		}		
	}
}

if (CanShoot()) {
	Target();
	
	Shoot();
}


// Update light
with (light) {
	blend = c_white;
	x = other.x;
	y = other.y;
}
