if (live_enabled && live_call()) return live_result;

if (Game.debug) {
	// Bounding box
	draw_set_alpha(0.4);
	draw_set_color(c_red);
	draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, false);
	draw_set_alpha(1);

	// Healthbar
	draw_healthbar(x-30, bbox_top-20, x+30, bbox_top-10, 100*entity_hp/entity_max_hp, true, c_red, c_green, 0, true, true);
	
	// Wave info
	if (!friendly) {
		draw_set_color(c_white);
		draw_set_halign(fa_center);
		draw_text(x, y-80, "Wave: "+str(current_wave));		
	}
}

