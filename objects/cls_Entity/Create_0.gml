if (live_enabled && live_call()) return live_result;

#region Structs and Enums

	enum WAVE_LOOP_BEHAVIOR {
		LOOP,
		END,
		STICK_AT_LAST
	}
	
	Coord = function(_x,_y) constructor {
		x = _x;
		y = _y;
	}
	
	
	BulletWave = function() constructor {
		bullet_objects = [];
		bullet_angles = [];
		bullet_timers = [];
		bullet_current_timers = [];
		
		add = function(_object, _angle, _timer, _start_timers=_timer) {
			array_push(bullet_objects, _object);
			array_push(bullet_angles, _angle);
			array_push(bullet_timers, _timer);
			array_push(bullet_current_timers, _start_timers);
		}
		
		tick = function() {
			var _n = array_length(bullet_current_timers);
			for (var _i=0; _i<_n; _i++) {
				bullet_current_timers[_i]--;
				if (bullet_current_timers[_i] < 0) {
					bullet_current_timers[_i] = bullet_timers[_i];
				}
			}
		}
	}

#endregion 

#region Attributes

	entity_speed_per_second = 240 * PX_PER_SEC;
	entity_max_hp = 3;	
	entity_hp = entity_max_hp;
		
	friendly = true;
	player_index = 0;
	
	waves = [];
	current_wave_time = 0;
	wave_loop_behavior = WAVE_LOOP_BEHAVIOR.LOOP;	
	wave_end_condition_functions = [];
	current_wave = 0;

#endregion

#region Methods

	GetInput = function() {		
		
	}
	
	CanMove = function() {
		return true;
	}
	
	Move = function() {
	}
	
	CanDash = function() {
		return true;
	}
	
	Dash = function() {
	}
			
	Target = function() {
	}
	
	CanShoot = function() {
		
	}
	
	Shoot = function() {
		
	}
		
	
	IsInvincible = function() {
		return false;
	}
	
	Die = function() {
		instance_destroy();
	}
	
	IsItTimeToShoot = function() {
		// Check if it's time to shoot at the current wave
		var _n = array_length(waves);
		if (_n>0) {
			var _m = array_length(waves[current_wave].bullet_current_timers);
			var _found = false;
			var _i = 0;
			while (_i<_m && !_found) {
				if (waves[current_wave].bullet_current_timers[_i] == 0) {
					_found = true;
				}
				else {
					_i++;
				}
			}
			return _found;
		}
		else {
			return false;
		}
	}
	
	ShootWave = function() {
		// Check if it's time to shoot at the current wave		
		var _n = array_length(waves[current_wave].bullet_current_timers);				
		for (var _i=0; _i<_n; _i++) {
			if (waves[current_wave].bullet_current_timers[_i] == 0) {
				with (instance_create_layer(x, y, "lyr_Instances", waves[current_wave].bullet_objects[_i])) {
					bullet_entity_id = other.id;
					bullet_friendly = other.friendly;
					bullet_direction = other.waves[other.current_wave].bullet_angles[_i];
					bullet_drift_function = function() { return 0; };
				}
			}
		}
	}

#endregion


#region Light

	light = new BulbLight(LightRenderer.renderer, spr_Light, 0, x, y);
	with (light) {
		blend = c_white;
		xscale = 1/8;
		yscale = 1/8;
		castShadows = false;
	}
	
#endregion
