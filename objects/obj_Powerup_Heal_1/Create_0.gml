if (live_enabled && live_call()) return live_result;
event_inherited();

ApplyPowerup = function() {
	if (other.entity_hp < other.entity_max_hp) {
		other.entity_hp = min(other.entity_max_hp, other.entity_hp+1);
		instance_destroy();
	}
}
