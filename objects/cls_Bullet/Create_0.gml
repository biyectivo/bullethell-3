if (live_enabled && live_call()) return live_result;

#region Attributes

	bullet_direction = 0;
	
	bullet_speed_per_second = 600 * PX_PER_SEC;	
	bullet_drift_function = function() { return 0; };
	bullet_damage = 1;
	bullet_cooldown = 10;
	bullet_friendly = true;
	
	bullet_entity_id = noone;

#endregion


#region Methods

	IsOutOfBounds = function() {
		var _level_border_radius = Game.current_circle_radius - RADIUS_THRESHOLD;
		return !point_in_circle(x, y, room_width/2, room_height/2, _level_border_radius);	
	}

	OutOfBounds = function() {
		instance_destroy();
	}

	Damage = function() {
		if (bullet_entity_id != other.id) {			
			if (bullet_friendly != other.friendly && !other.IsInvincible()) {
				other.entity_hp -= bullet_damage;
				instance_destroy();
			}
		}		
	}

	Move = function() {
		x += lengthdir_x(bullet_speed_per_second, bullet_direction+bullet_drift_function());
		y += lengthdir_y(bullet_speed_per_second, bullet_direction+bullet_drift_function());
	}
	
	ScaleRotate = function() {
	}
	
#endregion


#region Light

	light = new BulbLight(LightRenderer.renderer, spr_Light, 0, x, y);
	with (light) {
		blend = c_gray;
		xscale = 1/16;
		yscale = 1/16;
		castShadows = false;
	}

#endregion
