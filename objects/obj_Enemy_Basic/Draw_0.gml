if (live_enabled && live_call()) return live_result;

draw_set_alpha(0.5);
draw_ellipse_color(x-sprite_width/3, bbox_bottom, x+sprite_width/3, bbox_bottom+12, c_black, c_black, false);
draw_set_alpha(1);

draw_self();

event_inherited();
