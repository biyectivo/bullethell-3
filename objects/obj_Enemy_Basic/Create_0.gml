if (live_enabled && live_call()) return live_result;

event_inherited();


#region Enemy-specific attributes

	friendly = false;
	entity_speed_per_second = 60 * PX_PER_SEC;
	entity_max_hp = 30;	
	entity_hp = entity_max_hp;
	
	// Waves
	var _wave = new BulletWave();
	for (var _i=0; _i<360; _i += 15) {
		_wave.add(obj_Bullet_Green, _i, 45);
	}
	array_push(waves, _wave);
	array_push(wave_end_condition_functions, function() { return current_wave_time == 240; });
	
	/*
	// Test of second wave
	var _wave = new BulletWave();
	for (var _i=0; _i<360; _i += 45) {
		_wave.add(obj_Bullet_Basic, _i, 10);
		_wave.add(obj_Bullet_Basic, _i, 20);
	}
	array_push(waves, _wave);
	array_push(wave_end_condition_functions, function() { return current_wave_time == 480; });
	
	wave_loop_behavior = WAVE_LOOP_BEHAVIOR.LOOP;
	*/
	
#endregion

#region Enemy-specific methods

	CanMove = function() {
		return true;
	}

	Move = function() {
		y = ystart+30*sin(1/300*get_timer()/1000);
	}

	CanDash = function() {
		return false;
	}

	Target = function() {
	
	}

	CanShoot = function() {
		return IsItTimeToShoot();
	}
	
	Shoot = function() {
		ShootWave();
	}

	IsInvincible = function() {
		return false;
	}
	
#endregion


light.blend = c_lime;
