if (live_enabled && live_call()) return live_result;
/// @description Create the light
light = new BulbLight(LightRenderer.renderer, spr_Light, 0, x, y);
with (light) {
	blend = #bbbbbb;
	xscale = 2;
	yscale = 2;
	castShadows = false;
}
