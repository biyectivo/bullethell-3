#region Macros
	
	#macro	live_enabled		true

	#macro	DEVICE				0
	#macro	GUI_MOUSE_X			device_mouse_x(DEVICE)
	#macro	GUI_MOUSE_Y			device_mouse_y(DEVICE)
	#macro	GUI_WIDTH			display_get_gui_width()
	#macro	GUI_HEIGHT			display_get_gui_height()
	#macro	CAM					view_camera[0]
	#macro	GP_THRESHOLD		0.3
								
	#macro	print				show_debug_message
	#macro	str					string
								
	#macro	PX_PER_SEC			1/game_get_speed(gamespeed_fps)
	
	#macro	DASH_EFECT_LENGTH	8
	#macro	RADIUS_THRESHOLD	20
	
#endregion

#region Game functions

	function InitializeGameVariables() {
		
		debug = false;
		paused = false;
		god_mode = true;
		
		UpdateGamepads()
		
		num_players = 1;
		
		control_move_up =		[ord("W"),		gp_axislv];
		control_move_down =		[ord("S"),		gp_axislv];
		control_move_right =	[ord("D"),		gp_axislh];
		control_move_left =		[ord("A"),		gp_axislh];
		control_shoot_up =		[vk_up,			gp_face4];
		control_shoot_down =	[vk_down,		gp_face1];
		control_shoot_right =	[vk_right,		gp_face2];
		control_shoot_left =	[vk_left,		gp_face3];
		control_dash =			[vk_space,		gp_shoulderr];
		control_special =		[vk_rcontrol,	gp_shoulderl];
		control_pause =			[vk_escape,		gp_start];
				
		palette_surface_Player1 = -1;
		palette_surface_Player2 = -1;
		
	}

	function ResetGameVariables() {
		
		seed = randomize();
		random_set_seed(seed);
		
		current_circle_radius = room_height/2 - RADIUS_THRESHOLD;
		
		game_score = [0,0];
		current_game_step = 0;
		
	}
	
	function UpdateGamepads() {
		num_gamepads = gamepad_get_device_count();
		var _i=0;
		var _found = false;
		while (_i<num_gamepads && !_found) {
			if (gamepad_is_connected(_i)) {
				_found = true;
			}
			else {
				_i++;
			}
		}
		if (_found) {
			player_controls = [-1, _i];
		}
		else {
			player_controls = [-1, -9];
		}
	}

#endregion

#region Helper functions

	function array_find(_array, _value) {
		var _found = false;
		var _i = 0;
		var _n = array_length(_array);
		if (!is_array(_array) || _n == 0) {
			return -1;
		}
		else {
			while (_i<_n && !_found) {
				if (_array[_i] == _value) {
					_found = true;
				}
				else {
					_i++;
				}
			}
			return _found ? _i : -1;
		}
	}
	
	///@function				array_resize_from_end(array, size)
	///@description				resizes an array from the end
	///@param		{array}		array		the array
	///@param		{int}		size		the new size to resize to
	function array_resize_from_end(_array, _new_size) {
		var _n = array_length(_array);
		if (_new_size < _n) {		
			for (var _i=0; _i<_new_size; _i++) {
				_array[@_i] = _array[_i+_n-_new_size];
			}
			array_resize(_array, _new_size);
		}
	}

#endregion
