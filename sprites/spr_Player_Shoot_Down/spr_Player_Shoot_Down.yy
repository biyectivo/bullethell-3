{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 29,
  "bbox_top": 1,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"bf5865f0-7992-44a6-ad8c-3ddba9630890","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf5865f0-7992-44a6-ad8c-3ddba9630890","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"LayerId":{"name":"8a3dca7a-29f8-43c2-9cb1-af1fefe09e81","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Shoot_Down","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"resourceVersion":"1.0","name":"bf5865f0-7992-44a6-ad8c-3ddba9630890","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8e86ec72-0d4b-4449-859b-312fb0d14fc0","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8e86ec72-0d4b-4449-859b-312fb0d14fc0","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"LayerId":{"name":"8a3dca7a-29f8-43c2-9cb1-af1fefe09e81","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Shoot_Down","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"resourceVersion":"1.0","name":"8e86ec72-0d4b-4449-859b-312fb0d14fc0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Player_Shoot_Down","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"01796415-4a94-40fd-a2b4-635bd0145a08","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf5865f0-7992-44a6-ad8c-3ddba9630890","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b2dfab08-ea12-4a30-b22c-d6c7133d37dc","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8e86ec72-0d4b-4449-859b-312fb0d14fc0","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Player_Shoot_Down","path":"sprites/spr_Player_Shoot_Down/spr_Player_Shoot_Down.yy",},
    "resourceVersion": "1.4",
    "name": "spr_Player_Shoot_Down",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8a3dca7a-29f8-43c2-9cb1-af1fefe09e81","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Player_Shoot_Down",
  "tags": [],
  "resourceType": "GMSprite",
}